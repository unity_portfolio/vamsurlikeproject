using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{
    [Header("# 플레이어 위치")]
    public Transform m_Target;

    [Header("# 카메라 이동속력")]
    public float m_MoveSpeed;

    /// <summary>
    /// 오프셋
    /// </summary>
    private Vector3 _OffsetVal;

    private void Start()
    {
        // 오프셋 설정
        _OffsetVal = transform.position - m_Target.position;
    }

    private void FixedUpdate()
    {
        // 카메라가 플레이어 중심으로 이동
        MoveCamera();
    }

    /// <summary>
    /// 카메라를 플레이어 위치로 동기화하여 이동시킵니다.
    /// </summary>
    private void MoveCamera()
    {
        Vector3 targetCameraPos = m_Target.position + _OffsetVal;

        transform.position = Vector3.Lerp(transform.position, targetCameraPos, m_MoveSpeed * Time.deltaTime);
    }

}
