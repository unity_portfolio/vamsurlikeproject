using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 플레이어와 가장 가까운 적 찾는 컴포넌트
/// </summary>
public class EnemyFinder : MonoBehaviour
{
    [Header("# 감지 범위")]
    public float m_Range;

    [Header("# 감지 레이어")]
    public LayerMask m_TargetLayer;

    /// <summary>
    /// 감지된 객체들
    /// </summary>
    public RaycastHit2D[] m_Targets;

    /// <summary>
    /// 가장 가까운 객체
    /// </summary>
    public Transform m_FirstTarget;

    private void FixedUpdate()
    {
        m_Targets = Physics2D.CircleCastAll(
            transform.position, m_Range, Vector2.zero, 0.0f, m_TargetLayer);

        m_FirstTarget = GetFirstTarget();
    }

    /// <summary>
    /// 가장 가까운 적을 찾는 함수
    /// </summary>
    /// <returns></returns>
    private Transform GetFirstTarget()
    {
        Transform firstTarget = null;

        float distance = 100;

        foreach(RaycastHit2D target in m_Targets)
        {
            Vector3 myPos = transform.position;
            Vector3 targetPos = target.transform.position;
            float currentDistance = Vector3.Distance(myPos, targetPos);

            if(distance > currentDistance) 
            { 
                distance = currentDistance;
                firstTarget = target.transform;
            }
        }

        return firstTarget;
    }
}
