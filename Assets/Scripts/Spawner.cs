using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 몬스터가 스폰되는 로직을 갖는 컴포넌트
/// </summary>
public class Spawner : MonoBehaviour
{
    /// <summary>
    /// 스폰위치들
    /// </summary>
    private Transform[] m_spawnPoint;

    /// <summary>
    /// 소환 레벨
    /// </summary>
    private int _Level;

    /// <summary>
    /// 소환 타이머
    /// </summary>
    private float _Timer;

    private float _LevelTime;

    /// <summary>
    /// 소환 데이터
    /// </summary>
    public SpawnData[] spawnDatas;

    private void Awake()
    {
        m_spawnPoint = GetComponentsInChildren<Transform>();
        _LevelTime = GameManager.instance.m_MaxGameTime / spawnDatas.Length;
    }

    private void Update()
    {
        if (!GameManager.instance.m_IsLive) return;

        _Timer += Time.deltaTime;

        // 게임 시간과 스폰데이터 갯수에 따른 레벨 설정
        _Level = Mathf.Min( Mathf.FloorToInt(GameManager.instance.m_GameTime / _LevelTime),spawnDatas.Length - 1);

        // 게임 진행시간이 소환될 시간보다 커지면 
        if(_Timer > spawnDatas[_Level].spawnTime)
        {
            // 타이머 초기화후 소환
            _Timer = 0;
            Spawn();
        }
    }

    /// <summary>
    /// 몬스터 소환
    /// </summary>
    private void Spawn()
    {
        // 몬스터 종류와 소환 위치를 랜덤으로 설정
        GameObject enemy = GameManager.instance.m_Pool.GetGameObject(Random.Range(0,2));
        enemy.transform.position = m_spawnPoint[Random.Range(1,m_spawnPoint.Length)].position;

        // 소환 레벨에 따른 몬스터 데이터 설정
        enemy.GetComponent<EnemyCharacter>().Initialize(spawnDatas[_Level]);
    }
}

