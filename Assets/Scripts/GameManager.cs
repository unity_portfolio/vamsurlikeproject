using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// 다른 클래스에서 다른 클래스로 접근할 수 있도록
/// GameManager 클래스 생성
/// </summary>
public class GameManager : MonoBehaviour
{
    /// <summary>
    /// GameManager 인스턴스
    /// </summary>
    public static GameManager instance;

    public bool m_IsLive;

    [Header("# 최대 게임 시간")]
    public float m_MaxGameTime;

    [Header("# 현재 게임 시간")]
    public float m_GameTime;

    [Header("# Player Info")]
    public int m_PlayerId;
    public float m_Hp;
    public float m_MaxHp;
    public int m_Level;
    public int m_Kill;
    public int m_Exp;
    public int[] m_NextExp = { 5, 10, 20, 40, 80, 160, 200, 240, 320, 400 };


    [Header("# 플레이어 객체")]
    public PlayerCharacter m_Player;

    [Header("# Pool Manager")]
    public PoolManager m_Pool;

    [Header("# LevelUp UI")]
    public LevelUp m_LevelUpUI;

    [Header("# Result UI")]
    public Result m_ResultUI;

    [Header("# Joystick UI")]
    public Transform m_Joystick;

    public GameObject m_EnemyCleaner;

    private void Awake()
    {
        instance = this;

        Application.targetFrameRate = 60;
    }
    private void Update()
    {
        if (!m_IsLive) return;

        m_GameTime += Time.deltaTime;

        if (m_GameTime > m_MaxGameTime)
        {
            m_GameTime = m_MaxGameTime;
            GameClear();
        }
    }

    /// <summary>
    /// 게임 시작 메서드, UI의 버튼 객체와 이벤트 바인딩
    /// </summary>
    /// <param name="id"></param>
    public void GameStart(int id)
    {
        m_PlayerId = id;

        m_Hp = m_MaxHp;

        m_Player.gameObject.SetActive(true);

        m_LevelUpUI.Select(m_PlayerId % 2);

        Resume();
    }

    /// <summary>
    /// 플레이어 사망시 호출되는 게임 오버 메서드
    /// </summary>
    public void GameOver()
    {
        StartCoroutine(GameOverRoutine());
    }

    /// <summary>
    /// 게임 클리어 메서드
    /// </summary>
    public void GameClear()
    {
        StartCoroutine(GameClearRoutine());
    }

    /// <summary>
    /// 다시하기, UI의 버튼 객체와 이벤트 바인딩
    /// </summary>
    public void GameRetry()
    {
        SceneManager.LoadScene(0);
    }

    /// <summary>
    /// 게임 종료, UI의 버튼 객체와 이벤트 바인딩
    /// </summary>
    public void GameExit()
    {
        Application.Quit();
    }

    /// <summary>
    /// 경험치를 얻어 레벨을 올림
    /// </summary>
    public void GetExp()
    {
        if(!m_IsLive) return;

        m_Exp++;

        if(m_Exp == m_NextExp[Mathf.Min(m_Level,m_NextExp.Length-1)])
        {
            m_Level++;
            m_Exp = 0;

            m_LevelUpUI.Show();
        }
    }

    /// <summary>
    /// 게임 정지 메서드, 게임이 끝나거나 레벨업 할 경우 일시 정지
    /// </summary>
    public void Stop()
    {
        m_IsLive = false;
        Time.timeScale = 0;
        m_Joystick.localScale = Vector3.zero;
    }

    /// <summary>
    /// 게임 진행 메서드, 일시 정지후 다시 진행될수 있도록 설계
    /// </summary>
    public void Resume()
    {
        m_IsLive = true;
        Time.timeScale = 1;
        m_Joystick.localScale = Vector3.one;
    }

    /// <summary>
    /// 게임오버 코루틴
    /// </summary>
    /// <returns></returns>
    private IEnumerator GameOverRoutine()
    {
        m_IsLive = false;

        yield return new WaitForSeconds(0.5f);

        m_ResultUI.gameObject.SetActive(true);
        m_ResultUI.Over();

        Stop();
    }

    /// <summary>
    /// 게임 클리어 코루틴
    /// </summary>
    /// <returns></returns>
    private IEnumerator GameClearRoutine()
    {
        m_IsLive = false;
        m_EnemyCleaner.SetActive(true);

        yield return new WaitForSeconds(0.5f);

        m_ResultUI.gameObject.SetActive(true);
        m_ResultUI.Clear();

        Stop();
    }
}
