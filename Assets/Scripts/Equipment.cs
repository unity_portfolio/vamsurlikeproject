using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 플레이어의 장비(신발, 장갑)
/// </summary>
public class Equipment : MonoBehaviour
{
    /// <summary>
    /// 아이템 종류
    /// </summary>
    public ItemData.ItemType m_Type;

    /// <summary>
    /// 능력치 상승 비율
    /// </summary>
    public float m_Rate;

    /// <summary>
    /// 아이템 정보 초기화
    /// </summary>
    /// <param name="data"></param>
    public void Initialize(ItemData data)
    {
        name = "Equip " + data.itemId;
        // 오브젝트 생성 시 계층구조 설정
        transform.parent = GameManager.instance.m_Player.transform;
        transform.localPosition = Vector3.zero;

        // 아이템 종류 설정
        m_Type = data.itemType;

        // 레벨에 따른 능력치 증가를 위한 초기 설정
        m_Rate = data.damages[0];

        // 아이템 종류별 개별 능력치 상승 조절
        ApplyEquipment();
    }

    /// <summary>
    /// 장비 레벨에 따른 능력치 상승률 설정
    /// </summary>
    /// <param name="rate"></param>
    public void LevelUp(float rate)
    {
        m_Rate = rate;
        ApplyEquipment();
    }

    /// <summary>
    /// 장비 종류에 따른 능력치 상승
    /// </summary>
    private void ApplyEquipment()
    {
        switch (m_Type)
        {
            // 장갑
            case ItemData.ItemType.Glove:
                // 회전무기 회전 스피드 상승
                // 총알 연사력 증가
                RateUp();
                break;
            // 신발
            case ItemData.ItemType.Shoe:
                // 이동 속도
                SpeedUp();
                break;
        }
    }

    /// <summary>
    /// 회전무기 회전 속도 상승
    /// 총알 연사력 상승
    /// </summary>
    private void RateUp()
    {
        Weapon[] weapons = transform.parent.GetComponentsInChildren<Weapon>();

        foreach (Weapon weapon in weapons)
        {
            switch(weapon.m_Id)
            {
                // 회전무기
                case 0:
                    float speed = 150 * Ability.BulletSpeed;
                    weapon.m_Speed = speed + (speed * m_Rate);
                    break;
                // 총알
                default:
                    speed = 0.5f * Ability.BulletRate;
                    weapon.m_Speed = speed * (1.0f - m_Rate);
                    break;
            }
        }
    }

    /// <summary>
    /// 이동 속력 증가
    /// </summary>
    private void SpeedUp()
    {
        float speed = 3.0f * Ability.Speed;
        GameManager.instance.m_Player.m_Speed += speed * m_Rate;
    }
}
