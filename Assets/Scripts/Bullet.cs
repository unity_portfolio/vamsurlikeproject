using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 무기 오브젝트들이 가지는 컴포넌트
/// </summary>
public class Bullet : MonoBehaviour
{
    [Header("Bullet Info")]
    public float m_Damage;
    public int m_Per;

    private Rigidbody2D _Rigid;

    private void Awake()
    {
        _Rigid = GetComponent<Rigidbody2D>();
    }

    /// <summary>
    /// 무기 정보 초기화
    /// </summary>
    /// <param name="damage">데미지</param>
    /// <param name="per">관통력</param>
    /// <param name="direction">총알 발사 방향</param>
    public void Initialize(float damage, int per, Vector3 direction)
    {
        m_Damage = damage;
        m_Per = per;

        if(per >= 0)
        {
            _Rigid.velocity = direction * 15f;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // 충돌체 태그가 Enemy가 아니거나 관통력이 -100이면 호출 종료
        if (!collision.CompareTag("Enemy") || m_Per == -100) return;

        // 총알이 관통 될수록 관통력 감소
        m_Per--;

        // 관통력이 0보다 작게 되면 오브젝트 풀링을 위해 비활성화
        if(m_Per < 0)
        {
            _Rigid.velocity = Vector2.zero;
            gameObject.SetActive(false);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        // 충돌체 태그가 Area가 아니거나 관통력이 -100이면 호출 종료
        if (!collision.CompareTag("Area") || m_Per == -100) return;

        gameObject.SetActive(false);
    }
}
