using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUp : MonoBehaviour
{
    private RectTransform _Rect;

    private Item[] _Items;

    private void Awake()
    {
        _Rect = GetComponent<RectTransform>();
        _Items = GetComponentsInChildren<Item>(true);
    }

    /// <summary>
    /// 레벨 업 UI 활성화
    /// </summary>
    public void Show()
    {
        Next();
        // 원래 크기로 적용
        _Rect.localScale = Vector3.one;
        // 게임 일시 정지
        GameManager.instance.Stop();
    }

    /// <summary>
    /// 레벨업 UI 비활성화, 각 아이템 선택 버튼과 이벤트 바인딩
    /// </summary>
    public void Hide()
    {
        // 크기를 0으로 줄여 안보이도록 설정
        _Rect.localScale = Vector3.zero;
        // 게임 진행
        GameManager.instance.Resume();
    }

    public void Select(int index)
    {
        _Items[index].OnClick();
    }

    /// <summary>
    /// 아이템 선택 및 적용 메서드
    /// </summary>
    private void Next()
    {
        foreach (Item item in _Items)
        {
            item.gameObject.SetActive(false);
        }

        int[] ran = new int[3];
        while(true)
        {
            // 최대 3가지 선택지를 보일 수 있도록 설정
            ran[0] = Random.Range(0, _Items.Length);
            ran[1] = Random.Range(0, _Items.Length);
            ran[2] = Random.Range(0, _Items.Length);

            // 중복 방지
            if (ran[0] != ran[1] && ran[1] != ran[2] && ran[0] != ran[2]) break;
        }

        for(int i = 0; i<ran.Length; ++i)
        {
            Item ranItem = _Items[ran[i]];

            // 뽑힌 아이템이 최대 레벨일 경우
            if(ranItem.m_Level == ranItem.m_Data.damages.Length)
            {
                // 힐링 아이템이 활성화
                _Items[4].gameObject.SetActive(true);
            }
            else
            {
                ranItem.gameObject.SetActive(true);
            }

        }
    }
}
