using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Result : MonoBehaviour
{
    public GameObject[] m_Titles;

    public void Over()
    {
        m_Titles[0].SetActive(true);
    }

    public void Clear()
    {
        m_Titles[1].SetActive(true);
    }
}
