using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 플레이어 총 무기 시각화 컴포넌트
/// </summary>
public class Hand : MonoBehaviour
{
    /// <summary>
    /// 총 무기 스프라이트
    /// </summary>
    public SpriteRenderer m_Sprite;

    /// <summary>
    /// 플레이어 스프라이트
    /// </summary>
    private SpriteRenderer _PlayerSprite;

    /// <summary>
    /// 기본 상태 로컬회전값
    /// </summary>
    private Quaternion _Rotate = Quaternion.Euler(0, 0, -35);

    /// <summary>
    /// 반전 상태 로컬회전값
    /// </summary>
    private Quaternion _ReverseRotate = Quaternion.Euler(0, 0, 35);

    /// <summary>
    /// 기본 상태 로컬위치값
    /// </summary>
    private Vector3 _Position = new Vector3(0.1f, -0.2f, 0);

    /// <summary>
    /// 반전 상태 로컬위치값
    /// </summary>
    private Vector3 _ReversePosition = new Vector3(-0.1f, -0.2f, 0);

    private void Awake()
    {
        _PlayerSprite = GetComponentsInParent<SpriteRenderer>()[1];
    }

    private void LateUpdate()
    {
        // 플레이어 스프라이트의 flipX 값
        // 이 값은 오른쪽을 향할 떄는 false, 왼쪽을 향할 때는 true
        bool isReverse = _PlayerSprite.flipX;

        transform.localRotation = isReverse ? _ReverseRotate : _Rotate;
        transform.localPosition = isReverse ? _ReversePosition : _Position;
        m_Sprite.flipX = isReverse;

    }
}
