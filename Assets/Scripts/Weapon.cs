using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 근거리 원거리 무기 종류 설정 및
/// 소환 위치를 담당하는 컴포넌트
/// </summary>
public class Weapon : MonoBehaviour
{
    [Header("# Weapon Info")]
    public int m_Id;            // 근거리 회전 : 0, 원거리 : 1
    public int m_PrefabId;      // Pool Manager 가 풀링하는 프리팹들 중 2번엔 근거리, 3번엔 원거리
    public float m_Damage;      // 무기 데미지
    public int m_Count;         // 근거리 전용 시작 무기 갯수
    public float m_Speed;       // 근거리 무기 회전 속력, 원거리 무기 발사 텀

    /// <summary>
    /// 총알 발사 타이머
    /// </summary>
    private float _Timer;

    private PlayerCharacter _PlayerCharacter;

    private void Awake()
    {
        _PlayerCharacter = GameManager.instance.m_Player;
    }

    private void Update()
    {
        if (!GameManager.instance.m_IsLive) return;

        switch (m_Id)
        {
            // 근거리 회전 무기인 경우
            case 0:
                // 회전
                transform.Rotate(Vector3.back * m_Speed * Time.deltaTime);
                break;

            // 원거리 무기 인 경우
            default:
                _Timer += Time.deltaTime;

                if(_Timer > m_Speed)
                {
                    _Timer = 0.0f;
                    Fire();
                }
                break;
        }
    }

    /// <summary>
    /// id 에 따른 무기 초기값 세팅
    /// </summary>
    public void Initialize(ItemData data)
    {
        // Basic Set
        name = "Weapon " + data.itemId;
        transform.parent = _PlayerCharacter.transform;
        transform.localPosition = Vector3.zero;

        // Property Set
        m_Id = data.itemId;
        m_Damage = data.baseDamage * Ability.Damage;
        m_Count = data.baseCount + Ability.Count;

        for(int i = 0; i< GameManager.instance.m_Pool.m_ObjectPrefabs.Length; ++i)
        {
            if(data.projectile == GameManager.instance.m_Pool.m_ObjectPrefabs[i])
            {
                m_PrefabId = i;
                break;
            }
        }

        switch(m_Id)
        {
            // 근거리 회전 무기
            case 0:
                // 속력 초기화
                m_Speed = 150.0f * Ability.BulletSpeed;
                // 소환 위치 초기화
                SetPosition();
                break;

            // 원거리 무기
            default:
                // 발사 텀 설정
                m_Speed = 0.3f * Ability.BulletRate;
                break;
        }

        // 원거리 무기 보유 시 스프라이트 활성화
        if(m_Id == 1)
        {
            Hand hand = _PlayerCharacter.hand;
            hand.m_Sprite.sprite = data.hand;
            hand.gameObject.SetActive(true);
        }

        _PlayerCharacter.BroadcastMessage("ApplyEquipment",SendMessageOptions.DontRequireReceiver);
    }

    public void LevelUp(float damage, int count)
    {
        m_Damage = damage * Ability.Damage;
        m_Count += count;

        if(m_Id == 0)
            SetPosition();

        _PlayerCharacter.BroadcastMessage("ApplyEquipment",SendMessageOptions.DontRequireReceiver);
    }

    /// <summary>
    /// 회전 무기 위치 초기화
    /// </summary>
    private void SetPosition()
    {
        for(int i = 0; i < m_Count; ++i)
        {
            Transform spinWeapon; 

            // 근거리 무기 스폰 계층 구조 설정
            if(i < transform.childCount)
            {
                spinWeapon = transform.GetChild(i);
            }
            else
            {
                spinWeapon = GameManager.instance.m_Pool.GetGameObject(m_PrefabId).transform;
                spinWeapon.parent = transform;
            }

            spinWeapon.localPosition = Vector3.zero;
            spinWeapon.localRotation = Quaternion.identity;

            // 무기 갯수에 따라 일정 간격으로 세팅
            Vector3 rotation = Vector3.forward * 360 * i / m_Count;
            spinWeapon.Rotate(rotation);

            // 오브젝트의 오프셋 적용
            spinWeapon.Translate(spinWeapon.up * 2f, Space.World);

            spinWeapon.GetComponent<Bullet>().Initialize(m_Damage, -100, Vector3.zero); // per 값이 -100 이면 무한 관통
        }
    }

    /// <summary>
    /// 플레이어와 가장 가까운 적 캐릭터한테 총알 발사
    /// </summary>
    private void Fire()
    {
        // 플레이어와 가장 가까운 타겟에만 적용
        if (!_PlayerCharacter.enemyFinder.m_FirstTarget) return;

        // 가장 가까운 타겟 위치
        Vector3 targetPosition = _PlayerCharacter.enemyFinder.m_FirstTarget.position;

        // 방향
        Vector3 direction = (targetPosition - transform.position).normalized;

        Transform bullet = GameManager.instance.m_Pool.GetGameObject(m_PrefabId).transform;
        // 총알 위치 초기화
        bullet.position = transform.position;
        // 총알 앞쪽이 적을 향하도록 회전
        bullet.rotation = Quaternion.FromToRotation(Vector3.right,direction);

        // 적을 향해 발사
        bullet.GetComponent<Bullet>().Initialize(m_Damage, m_Count, direction);
    }

}
