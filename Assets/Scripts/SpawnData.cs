


using UnityEngine;

[System.Serializable]
public struct SpawnData
{
    [Header("# 소환 주기")]
    public float spawnTime;

    [Header("# 체력")]
    public int hp;

    [Header("# 속력")]
    public float speed;
}