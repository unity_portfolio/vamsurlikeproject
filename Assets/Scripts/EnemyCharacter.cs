using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using UnityEngine;

public class EnemyCharacter : MonoBehaviour
{
    [Header("# Enemy Info")]
    public float m_Speed;
    public float m_Hp;
    public float m_MaxHp;

    /// <summary>
    /// 플레이어의 Rigidbody 컴포넌트
    /// </summary>
    private Rigidbody2D _Target;

    /// <summary>
    /// 생존 여부를 나타냅니다.
    /// </summary>
    private bool _IsLive;

    /// <summary>
    /// 적 캐릭터 Rigidbody
    /// </summary>
    private Rigidbody2D rigid;

    private Collider2D coll;

    /// <summary>
    /// 적 캐릭터 SpriteRenderer
    /// </summary>
    private SpriteRenderer sprite;

    /// <summary>
    /// 적 캐릭터 Animator
    /// </summary>
    private Animator animator;

    private WaitForFixedUpdate _Wait;

    private void Awake()
    {
        // Get Component
        rigid = GetComponent<Rigidbody2D>();
        coll = GetComponent<Collider2D>();
        sprite = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();

        _Wait = new WaitForFixedUpdate();
    }

    private void FixedUpdate()
    {
        if (!GameManager.instance.m_IsLive) return;

        // 플레이어 추적 
        FollowPlayer();
    }

    private void LateUpdate()
    {
        if (!GameManager.instance.m_IsLive) return;

        // flipX
        FlipX();
    }

    private void OnEnable()
    {
        // 적 생성시 목표를 플레이어로 설정
        _Target = GameManager.instance.m_Player.GetComponent<Rigidbody2D>();

        // 적이 풀링에서 재활용 될 때 초기화
        _IsLive = true;
        coll.enabled = true;
        rigid.simulated = true;
        sprite.sortingOrder = 2;
        animator.SetBool("_Dead", false);
        m_Hp = m_MaxHp;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // 총알과 충돌하거나 살아있는 경우에만 적용
        if (!collision.CompareTag("Bullet") || !_IsLive) return;

        // 체력 처리
        m_Hp -= collision.GetComponent<Bullet>().m_Damage;

        // 추적이 시작되면 함수 호출
        StartCoroutine(KnockBack());

        if(m_Hp > 0)
        {
            // 피격 애니메이션 적용
            animator.SetTrigger("_Hit");
        }
        else
        {
            // 사망 처리
            m_Hp = 0;
            _IsLive = false;
            coll.enabled = false;
            rigid.simulated = false;
            sprite.sortingOrder = 1;
            animator.SetBool("_Dead", true);

            // 적 사망시 킬 수 증가
            GameManager.instance.m_Kill++;

            // 경험치 상승
            GameManager.instance.GetExp();
        }
    }

    /// <summary>
    /// 적이 플레이어를 추적하면서 적용되는
    /// 넉백 코루틴
    /// </summary>
    /// <returns></returns>
    private IEnumerator KnockBack()
    {
        yield return _Wait;

        Vector3 playerPos = GameManager.instance.m_Player.transform.position;
        Vector3 direction = transform.position - playerPos;
        
        // 넉백 적용
        rigid.AddForce(direction.normalized * 3.0f, ForceMode2D.Impulse);
    }
    
    /// <summary>
    /// 사망 애니메이션 실행을 위해 애니메이터 에서 호출 이벤트 설정
    /// </summary>
    private void AnimEvent_Dead()
    {
        gameObject.SetActive(false);
    }

    /// <summary>
    /// 플레이어를 추적하게하는 메서드
    /// </summary>
    private void FollowPlayer()
    {
        // 죽은 경우 함수 호출 종료
        if (!_IsLive || animator.GetCurrentAnimatorStateInfo(0).IsName("Anim_Hit")) return;

        // 목표로 가는 방향
        Vector2 direction = _Target.position - rigid.position;

        // 목표로 가는 속도
        Vector2 destination = direction.normalized * m_Speed * Time.fixedDeltaTime;

        // 추적
        rigid.MovePosition(rigid.position + destination);

        // 목표 지점 도착하면 속도 0으로 설정
        rigid.velocity = Vector2.zero;
    }

    /// <summary>
    /// 적 캐릭터를 x축 방향에 맞게 flip
    /// </summary>
    private void FlipX()
    {
        if (!_IsLive) return;
        sprite.flipX = _Target.position.x < rigid.position.x;
    }

    /// <summary>
    /// 소환 데이터에 따른 적 캐릭터 설정 초기화
    /// </summary>
    /// <param name="spawnData"></param>
    public void Initialize(SpawnData spawnData)
    {
        m_Speed = spawnData.speed;
        m_MaxHp = spawnData.hp;
        m_Hp = spawnData.hp;
    }
}
