using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 오브젝트 풀링 매니저 클래스
/// </summary>
public class PoolManager : MonoBehaviour
{
    [Header("# 오브젝트 프리팹")]
    public GameObject[] m_ObjectPrefabs;

    /// <summary>
    /// 오브젝트를 담을 리스트들
    /// </summary>
    private List<GameObject>[] _ObjectPools;

    private void Awake()
    {
        // 적 프리팹 갯수에 맞게 리스트들 초기화
        _ObjectPools = new List<GameObject>[m_ObjectPrefabs.Length];

        for(int i = 0; i<_ObjectPools.Length; ++i)
        {
            _ObjectPools[i] = new();
        }
    }

    public GameObject GetGameObject(int index)
    {
        GameObject poolingObj = null;

        // 오브젝트 풀링 속에 비활성화된 오브젝트가 있다면
        // 오브젝트로 적용하고 활성화
        foreach(GameObject obj in _ObjectPools[index])
        {
            if (!obj.activeSelf)
            {
                poolingObj = obj;
                poolingObj.SetActive(true);
                break;
            }
        }

        // poolingObj 없다면
        if (!poolingObj)
        {
            // 오브젝트 풀에 poolingObj 추가
            poolingObj = Instantiate(m_ObjectPrefabs[index],transform);
            _ObjectPools[index].Add(poolingObj);
        }

        return poolingObj;
    }
}
