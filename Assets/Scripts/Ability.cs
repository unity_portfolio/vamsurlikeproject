using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ability : MonoBehaviour
{
    public static float Speed
    {
        get { return GameManager.instance.m_PlayerId == 1 ? 1.1f : 1f; }
    }

    public static float BulletSpeed
    {
        get { return GameManager.instance.m_PlayerId == 3 ? 1.1f : 1f; }
    }

    public static float BulletRate
    {
        get { return GameManager.instance.m_PlayerId == 3 ? 0.9f : 1f; }
    }

    public static float Damage
    {
        get { return GameManager.instance.m_PlayerId == 2 ? 1.2f : 1f; }
    }

    public static int Count
    {
        get { return GameManager.instance.m_PlayerId == 2 ? 1 : 0; }
    }
}
