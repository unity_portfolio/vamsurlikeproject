using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 아이템 상태 버튼 컴포넌트
/// </summary>
public class Item : MonoBehaviour
{
    [Header("# Item Info")]
    public ItemData m_Data;
    public int m_Level;
    public Weapon m_Weapon;
    public Equipment m_Equipment;

    [Header("# UI Object")]
    public Image m_ItemIcon;
    public TMP_Text m_ItemLevel;
    public TMP_Text m_ItemName;
    public TMP_Text m_ItemDescription;

    private void Awake()
    {
        m_ItemIcon.sprite = m_Data.itmeIcon;
        m_ItemName.text = m_Data.itemName;

    }

    private void OnEnable()
    {
        m_ItemLevel.text = $"Lv.{m_Level+1:D2}";

        switch (m_Data.itemType)
        {
            case ItemData.ItemType.Spin:
            case ItemData.ItemType.Gun:
                m_ItemDescription.text = string.Format(m_Data.itemDesc, m_Data.damages[m_Level] * 100, m_Data.counts[m_Level]);
                break;
            case ItemData.ItemType.Glove:
            case ItemData.ItemType.Shoe:
                m_ItemDescription.text = string.Format(m_Data.itemDesc, m_Data.damages[m_Level] * 100);
                break;
            default:
                m_ItemDescription.text = string.Format(m_Data.itemDesc);
                break;
        }
    }

    /// <summary>
    /// 버튼 클릭 이벤트
    /// </summary>
    public void OnClick()
    {
        switch (m_Data.itemType)
        {
            // 무기류
            case ItemData.ItemType.Spin:
            case ItemData.ItemType.Gun:
                // 첫 클릭시
                if(m_Level == 0)
                {
                    // 오브젝트 생성 및 초기화
                    GameObject newWeapon = new GameObject();
                    m_Weapon = newWeapon.AddComponent<Weapon>();
                    m_Weapon.Initialize(m_Data);
                }
                // 그 다음 클릭 부터
                else
                {
                    float nextDamage = m_Data.baseDamage;
                    int nextCount = 0;

                    // 데미지 상승 및 갯수 , 연사력 증가 수치 설정
                    nextDamage += m_Data.baseDamage * m_Data.damages[m_Level];
                    nextCount += m_Data.counts[m_Level];

                    // 수치를 적용
                    m_Weapon.LevelUp(nextDamage, nextCount);
                }
                m_Level++;
                break;
            // 장비류
            case ItemData.ItemType.Glove:
            case ItemData.ItemType.Shoe:
                // 첫 클릭시 
                if(m_Level == 0)
                {
                    // 오브젝트 생성 및 초기화
                    GameObject newEquip = new GameObject();
                    m_Equipment = newEquip.AddComponent<Equipment>();
                    m_Equipment.Initialize(m_Data);
                }
                else
                {
                    // 능력치 상승 비율 수치 설정
                    float nextRate = m_Data.damages[m_Level];
                    // 수치 적용
                    m_Equipment.LevelUp(nextRate);
                }
                m_Level++;
                break;
            // 힐 아이템
            case ItemData.ItemType.Heal:
                // 회복
                GameManager.instance.m_Hp = GameManager.instance.m_MaxHp;
                break;
        }


        // 능력치가 최대 상승 했을 경우
        if(m_Level == m_Data.damages.Length)
        {
            // 버튼 비활성화
            GetComponent<Button>().interactable = false;
        }
    }
}