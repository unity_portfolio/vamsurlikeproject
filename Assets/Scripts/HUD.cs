using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static Cinemachine.DocumentationSortingAttribute;

/// <summary>
/// HUD 컴포넌트
/// </summary>
public class HUD : MonoBehaviour
{
    /// <summary>
    /// UI 종류 열거
    /// </summary>
    public enum InfoType
    {
        Exp,
        Level,
        Kill,
        Time,
        Health,
    }

    /// <summary>
    /// 현재 UI의 종류
    /// </summary>
    public InfoType m_Type;

    private TMP_Text _Text;

    private Image _Image;

    private void Awake()
    {
        _Text = GetComponent<TMP_Text>();
        _Image = GetComponent<Image>();
    }

    private void LateUpdate()
    {
        switch (m_Type)
        {
            // Exp
            case InfoType.Exp:
                float curExp = GameManager.instance.m_Exp;
                float maxExp = GameManager.instance.m_NextExp[Mathf.Min(GameManager.instance.m_Level, GameManager.instance.m_NextExp.Length - 1)];
                _Image.fillAmount = curExp / maxExp;
                break;
            // Level
            case InfoType.Level:
                _Text.text = GameManager.instance.m_Level.ToString();
                break;
            // Kill Score
            case InfoType.Kill:
                _Text.text = $"X {GameManager.instance.m_Kill}";
                break;
            // Timer
            case InfoType.Time:
                float remainTime = GameManager.instance.m_MaxGameTime - GameManager.instance.m_GameTime;
                int min = Mathf.FloorToInt(remainTime / 60);
                int sec = Mathf.FloorToInt(remainTime % 60);
                _Text.text = $"{min:D2}:{sec:D2}";
                break;
            // Player HP
            case InfoType.Health:
                float curHp = GameManager.instance.m_Hp;
                float maxHp = GameManager.instance.m_MaxHp;
                _Image.fillAmount = curHp / maxHp;
                break;
        }
    }


}
