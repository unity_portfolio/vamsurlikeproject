using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 선택한 오브젝트를 재배치하는 클래스
/// </summary>
public class Reposition : MonoBehaviour
{
    /// <summary>
    /// Collider 컴포넌트
    /// </summary>
    private Collider2D coll;

    private void Awake()
    {
        // Get Component
        coll = GetComponent<Collider2D>();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        // 플레이어가 가진 Area 범위 오브젝트가 아니라면 함수 호출 종료
        if (!collision.CompareTag("Area")) return;

        // 플레이어 위치
        Vector3 playerPos = GameManager.instance.m_Player.transform.position;
        
        // 오브젝트 위치
        Vector3 mapPos = transform.position;

        float dirX = playerPos.x - mapPos.x;
        float dirY = playerPos.y - mapPos.y;

        float diffX = Mathf.Abs(dirX);
        float diffY = Mathf.Abs(dirY);

        dirX = dirX > 0 ? 1 : -1;
        dirY = dirY > 0 ? 1 : -1;

        switch (transform.tag)
        {
            // 타일 맵 경우
            // 맵이 끊기지 않게 이동
            case "Ground":
                // 플레이어가  x축 방향으로 많이 움직일 경우
                if (diffX > diffY)
                    // 맵이 플레이어 x 축 방향으로 이동
                    transform.Translate(Vector3.right * dirX * 40.4f);
                // 플레이어가 y축 방향으로 많이 움직일 경우
                else if (diffX < diffY)
                    // 맵이 플레이어 y 축 방향으로 이동
                    transform.Translate(Vector3.up * dirY * 40.4f);

                break;

            // 적 캐릭터가 플레이어 시야에서 많이 멀어진 경우
            // 플레이어 주위의 일정 지점으로 재배치
            case "Enemy":
                // 적 캐릭터가 살아있을 때만
                if (coll.enabled)
                {
                    Vector3 distance = playerPos - mapPos;

                    Vector3 ran = new Vector3(Random.Range(-3, 3), Random.Range(-3, 3), 0);

                    transform.Translate(ran + distance * 2);
                }
                break;
        }

    }
}
