using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Hp바도 플레이어 위치에 동기화하여 카메라의 위치에 따라 이동하게 하는 컴포넌트
/// </summary>
public class FollowPlayer : MonoBehaviour
{
    private RectTransform _Rect;

    private void Awake()
    {
        _Rect = GetComponent<RectTransform>();
    }

    private void FixedUpdate()
    {
        _Rect.position = Camera.main.WorldToScreenPoint(GameManager.instance.m_Player.transform.position);
            
    }
}
