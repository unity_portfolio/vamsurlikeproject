using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerCharacter : MonoBehaviour
{
    [Header("# 이동 속력")]
    public float m_Speed;

    public RuntimeAnimatorController[] m_AnimCon;

    /// <summary>
    /// 입력 축 값
    /// </summary>
    private Vector2 _InputVector;

    /// <summary>
    /// Rigidbody 컴포넌트
    /// </summary>
    private Rigidbody2D _Rigid;

    /// <summary>
    /// Sprite Renderer 컴포넌트
    /// </summary>
    private SpriteRenderer _Sprite;

    /// <summary>
    /// Animator 컴포넌트
    /// </summary>
    private Animator _Anim;

    /// <summary>
    /// EnemyFinder 컴포넌트
    /// </summary>
    private EnemyFinder _EnemyFinder;

    /// <summary>
    /// Hand 컴포넌트
    /// </summary>
    private Hand _Hand; 

    /// <summary>
    /// EnemyFinder 프로퍼티
    /// </summary>
    public EnemyFinder enemyFinder => _EnemyFinder ?? (_EnemyFinder = GetComponent<EnemyFinder>());

    /// <summary>
    /// Hand 프로퍼티
    /// </summary>
    public Hand hand => _Hand ?? (_Hand = GetComponentInChildren<Hand>(true));

    /// <summary>
    /// 입력 축 값 프로퍼티
    /// </summary>
    public Vector2 inputVector => _InputVector;

    private void Awake()
    {
        // Get Component
        _Rigid = GetComponent<Rigidbody2D>();
        _Sprite = GetComponent<SpriteRenderer>();
        _Anim = GetComponent<Animator>();
    }


    private void FixedUpdate()
    {
        if (!GameManager.instance.m_IsLive) return;

        // 입력 축 값과 속력으로 속도를 갱신
        Vector2 velocity = _InputVector.normalized * m_Speed * Time.fixedDeltaTime;

        // 구한 속도 값으로 이동
        _Rigid.MovePosition(_Rigid.position + velocity);
    }

    private void LateUpdate()
    {
        if (!GameManager.instance.m_IsLive) return;

        // 이동 애니메이션 변수 값
        _Anim.SetFloat("_Speed", _InputVector.magnitude);

        // flipX
        FlipX();
    }

    private void OnEnable()
    {
        m_Speed *= Ability.Speed;

        _Anim.runtimeAnimatorController = m_AnimCon[GameManager.instance.m_PlayerId];
    }

    private void OnMove(InputValue axisValue)
    {
        _InputVector = axisValue.Get<Vector2>();
    }

    /// <summary>
    /// 움직이는 x축 방향으로 이미지 향하게 하는 메서드
    /// </summary>
    private void FlipX()
    {
        // 플레이어 스프라이트 방향 설정
        if (_InputVector.x != 0)
        {
            _Sprite.flipX = (_InputVector.x < 0);
        }

    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (!GameManager.instance.m_IsLive) return;

        GameManager.instance.m_Hp -= Time.deltaTime * 10;

        if(GameManager.instance.m_Hp < 0)
        {
            for (int i = 1; i < transform.childCount; ++i)
            {
                transform.GetChild(i).gameObject.SetActive(false);
            }

            _Anim.SetTrigger("_Dead");
            GameManager.instance.GameOver();
        }
    }
}
