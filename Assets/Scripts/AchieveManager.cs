using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AchieveManager : MonoBehaviour
{
    public GameObject[] m_LockCharacter;

    public GameObject[] m_UnlockCharacter;

    public GameObject m_UIAchieve;

    // UI가 유지되는 시간
    private WaitForSecondsRealtime _Wait;

    enum Achieve
    {
        UnlockChar2,
        UnlockChar3
    }

    Achieve[] achieves;

    private void Awake()
    {
        achieves = (Achieve[])Enum.GetValues(typeof(Achieve));

        _Wait = new WaitForSecondsRealtime(5);

        // 데이터 저장 키가 없는 경우 데이터 초기화
        if(!PlayerPrefs.HasKey("MyData"))
        {
            Initialize();
        }
    }

    private void Start()
    {
        UnlockCharacter();
    }

    private void LateUpdate()
    {
        // 업적 달성 여부 확인
        foreach(Achieve achieve in achieves)
        {
            CheckAchieve(achieve);
        }
    }

    /// <summary>
    /// 데이터 초기화
    /// </summary>
    private void Initialize()
    {
        // PlayerPrefs 생성
        PlayerPrefs.SetInt("MyData", 1);

        // 업적 미달성 상태로 설정
        foreach(Achieve achieve in achieves)
        {
            PlayerPrefs.SetInt(achieve.ToString(), 0);
        }
    }

    /// <summary>
    /// PlayerPrefs의 데이터를 확인하여 UI객체 설정
    /// </summary>
    private void UnlockCharacter()
    {
        for(int i = 0; i< m_LockCharacter.Length; ++i)
        {
            string achieveName = achieves[i].ToString();
            bool isUnlock = PlayerPrefs.GetInt(achieveName) == 1;
            m_LockCharacter[i].SetActive(!isUnlock);
            m_UnlockCharacter[i].SetActive(isUnlock);
        }
    }

    /// <summary>
    /// 업적 달성 여부 확인
    /// </summary>
    /// <param name="achieve"></param>
    private void CheckAchieve(Achieve achieve)
    {
        bool isAchieve = false;

        switch (achieve)
        {
            // 각 캐릭터의 해금 조건 나열
            case Achieve.UnlockChar2:
                isAchieve = GameManager.instance.m_Kill >= 500;
                break;
            case Achieve.UnlockChar3:
                isAchieve = GameManager.instance.m_GameTime == GameManager.instance.m_MaxGameTime;
                break;
        }

        // 캐릭터가 잠겨 있는 상태에서 업적을 달성한 경우
        if(isAchieve && PlayerPrefs.GetInt(achieve.ToString()) ==0)
        {
            // 업적 달성 상태로 변경
            PlayerPrefs.SetInt((achieve.ToString()), 1);

            for(int i = 0; i< m_UIAchieve.transform.childCount; ++i)
            {
                // 업적 달성 UI 생성을 위한 변수
                bool isActive = i == (int)achieve;

                // 업적 달성시 해당 UI 생성
                m_UIAchieve.transform.GetChild(i).gameObject.SetActive(isActive);
            }
            // 5초후 비활성화 되도록 설계
            StartCoroutine(AchieveRoutine());
        }
    }
    private IEnumerator AchieveRoutine()
    {
        m_UIAchieve.SetActive(true);
        yield return _Wait;
        m_UIAchieve.SetActive(false);
    }

}
